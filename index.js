

// Написати функцію filterBy(), яка прийматиме 2 аргументи. Перший аргумент - масив, який міститиме будь-які дані, другий аргумент - тип даних.
// Функція повинна повернути новий масив, який міститиме всі дані, які були передані в аргумент, за винятком тих, тип яких був переданий другим аргументом. Тобто якщо передати масив ['hello', 'world', 23, '23', null], і другим аргументом передати 'string', то функція поверне масив [23, null].

const arr = ['hello', 'world', 20, '23', null, 30, '30', true, 100, undefined, 30 , 111, 'some str']
function filterByDataType (arr, type) {
    let resultArray = [];
    arr.forEach((item) => {
        if (typeof item !== type ) {
            resultArray.push(item)
        } 
    })

    return resultArray
}
const resultArray = filterByDataType(arr, 'number');
console.log(resultArray)
